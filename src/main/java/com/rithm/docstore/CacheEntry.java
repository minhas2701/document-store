package com.rithm.docstore;

/**
 * CacheEntry class for holding metadata and value
 *
 */
final class CacheEntry implements Comparable<CacheEntry> {

	/** weight of cache entry for finding least evict */
	private Double weight = 0D;
	private Integer frquency = 0;

	private Object value = null;
	private Object Key = null;

	public CacheEntry(Object Key, Object value) {
		this.value = value;
		this.Key = Key;

	}

	/** compare weight */
	public int compareTo(CacheEntry element) {
		return weight.compareTo(element.getWeight());
	}

	public Double getWeight() {
		return weight;
	}

	/** weight = age + frequency */
	public void updateWeight(Double age) {
		weight = age + ++frquency;
	}

	public Object getValue() {
		return value;
	}

	public Object getKey() {
		return Key;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "[ key " + Key + " weight:" + weight + " frquency:" + frquency + " ]";

	}

}
