package com.rithm.docstore;

public final class DocStorage {

	private LfudaCache cache;
	private static final DocStorage INSTANCE = new DocStorage();

	private DocStorage() {
		cache = new LfudaCache(100);
	}

	public static DocStorage getInstance() {
		return INSTANCE;
	}

	public String getDocument(String request) {

		Object obj = cache.get(request);

		if (obj == null) {
			String doc = DocumentProducer.getDocument(request);
			if (doc != null)
				cache.store(request, doc);
			return doc;
		}

		// System.out.println(cache);
		return obj.toString();
	}

}
