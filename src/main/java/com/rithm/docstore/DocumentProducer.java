package com.rithm.docstore;

/**
 * As mentioned in problem document this class should not be altered so i add
 * this class exactly which mentioned in document.
 *
 */
public class DocumentProducer {
	
	public static String getDocument(String request) {
		String[] parts = request.split(" ");
		java.lang.StringBuilder sb = new java.lang.StringBuilder();
		int seed = request.length() + parts.length;
		while (sb.length() < 1000) {
			seed = (seed * 65793 + 4282663) % (1 << 23);
			if (((seed >> 8) & 1) == 0)
				sb.append(" ");
			else {
				seed = (seed * 65793 + 4282663) % (1 << 23);
				sb.append(parts[((seed >> 8) & 65535) % parts.length]);
			}
		}
		return sb.toString();
	}
	

	
}
