package com.rithm.docstore;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * LFU with Dynamic Aging (LFUDA) is algorithm for cache replacement policy.
 * LFUDA evicting value base on aging and frequency factor, we record object
 * with weight(aging + frequency) when hit on cache
 * 
 * 
 * @see <a
 *      href="https://en.wikipedia.org/wiki/Cache_replacement_policies#LFU_with_dynamic_aging_(LFUDA)"</a>
 * 
 * @author jawad
 * 
 */
final class LfudaCache implements ICache {

	private final int capacity;
	private Double age = 0D;
	private Map<Object, CacheEntry> cacheMap;
	private long hitCount;
	private long missCount;

	/**
	 * max size for cache value
	 * 
	 * @param capacity
	 * 
	 */
	public LfudaCache(int capacity) {
		if (capacity <= 0)
			throw new IllegalArgumentException("Cache size should be postive");

		this.capacity = capacity;
		cacheMap = new LinkedHashMap<Object, CacheEntry>();
	}

	public Object get(Object key) {
		if (key == null)
			throw new IllegalArgumentException("null key not allowed");

		CacheEntry entry = cacheMap.get(key);
		if (entry != null) {

			cacheMap.get(key).updateWeight(age);
			hitCount++;
			return entry.getValue();
		} else {
			missCount++;
			return null;
		}

	}

	/**
	 * When cache is full we emit the object on the base of minimum weight(aging +
	 * frequency) value
	 * 
	 */
	private synchronized void evict() {

		List<CacheEntry> evictEntrys = new LinkedList<CacheEntry>(cacheMap.values());
		Collections.sort(evictEntrys);
		CacheEntry evictEntry = evictEntrys.get(0);
		age = evictEntry.getWeight();
		cacheMap.remove(evictEntry.getKey());

	}

	public void store(Object key, Object value) {

		if (size() >= capacity)
			evict();

		cacheMap.put(key, new CacheEntry(key, value));
	}

	public int size() {

		return cacheMap.size();
	}

	public int capacity() {
		return capacity;
	}

	/**
	 *
	 * @return the percentage of cache requests hits
	 */
	public float hitRatePerc() {
		long reqeustCount = hitCount + missCount;

		return reqeustCount != 0 ? (hitCount * 100.0f) / reqeustCount : 0;
	}

	/**
	 * @return hit count on cached .
	 * 
	 */
	public long hitCount() {
		return hitCount;
	}

	/**
	 * @return miss count on cached .
	 * 
	 */
	public long hitMiss() {
		return missCount;
	}

	public String toString() {
		return cacheMap.toString();
	}

}
