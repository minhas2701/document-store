package com.rithm.docstore;

import java.util.Collection;

/**
 * The Interface Cache.
 *
 * @param <K> the key type
 * @param <V> the value type
 */

public interface ICache {
	
	/**
     * Puts the value with the specified key.
     *
     * @param key the key
     * @param value the value
     */
	void store(Object key, Object value);
    
    /**
     * Gets the value with the specified key.
     *
     * @param key the key
     * @return the value
     */
	Object get(Object key);
    
    /**
     * Calculates the Percentage.
     *
     * @return the Percentage
     */
	float  hitRatePerc();
    
    /**
     * Returns the cache size.
     * 
     * @return the size of the cache
     */
    public int size();
    
    /**
     *
     * @return the hitCount
     */
	public long hitCount();
	
    /**
     *
     * @return the hitMiss 
     */
	public long hitMiss();

}
