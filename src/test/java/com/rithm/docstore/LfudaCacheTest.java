package com.rithm.docstore;

import junit.framework.TestCase;

public class LfudaCacheTest extends TestCase {

	public void testLfudaCache() {
		ICache cache = new LfudaCache(3);
		cache.store(1, "1");
		cache.store(2, "2");
		cache.store(3, "3");
		// System.out.println(cache);
		cache.store(4, "4");
		cache.store(5, "5");
		// System.out.println(cache);

		assertEquals("3", cache.get(3));
		assertEquals("3", cache.get(3));
		cache.store(6, "6");
		assertEquals("3", cache.get(3));
		assertEquals(null, cache.get(4));

		// System.out.println(cache);

	}

	public void testGet() {
		ICache cache = new LfudaCache(3);
		assertEquals(null, cache.get(1));

		cache.store(1, "1");
		assertEquals("1", cache.get(1));
		assertEquals("1", cache.get(1));

		// System.out.println(cache);

	}

	public void testStore() {
		ICache cache = new LfudaCache(3);
		cache.store(1, "1");
		assertEquals("1", cache.get(1));

	}

	public void testSize() {
		ICache cache = new LfudaCache(3);
		cache.store(1, "1");
		cache.store(1, "1");
		cache.store(1, "1");
		cache.store(2, "2");

		assertNotSame(4, cache.size());
		assertEquals(2, cache.size());
	}

	public void testHitCount() {
		ICache cache = new LfudaCache(3);
		cache.store(1, "1");
		cache.store(2, "2");
		cache.get(3);
		cache.get(3);
		assertEquals(0, cache.hitCount());
		cache.get(1);
		cache.get(2);
		assertEquals(2, cache.hitCount());
		cache.get(2);
		cache.get(2);
		assertEquals(4, cache.hitCount());

	}

	public void testHitMiss() {
		ICache cache = new LfudaCache(3);
		cache.store(1, "1");
		cache.store(2, "2");
		cache.get(3);
		cache.get(3);
		assertEquals(2, cache.hitMiss());
		cache.get(1);
		cache.get(2);
		assertEquals(2, cache.hitMiss());
		cache.get(2);
		cache.get(3);
		assertEquals(3, cache.hitCount());

	}

	public void testHitRatePerc() {
		ICache cache = new LfudaCache(3);
		cache.store(1, "1");
		cache.store(2, "2");

		cache.get(3);
		cache.get(2);

		assertEquals(50, cache.hitRatePerc(), 0.8);
		cache.get(2);
		assertEquals(66, cache.hitRatePerc(), 0.8);

	}

}
