package com.rithm.docstore;

import junit.framework.TestCase;

public class DocumentProducerTest extends TestCase {

	public String getDocument(String request) {

		return DocumentProducer.getDocument(request);
	}

	public void test_simple() {
		String a = getDocument("simple");
		String b = getDocument("simple");
		assert (a.equals(b));
	}

	public void test_simple_neg() {
		String a = getDocument("simple neg");
		String b = getDocument("neg simple");
		assert (!a.equals(b));
	}

	public void test_repeated() {
		String a = getDocument("repeated");
		for (int i = 0; i < 200; i++) {
			String b = getDocument("repeated");
			assert (a.equals(b));
		}
	}

	public void test_misc() {
		for (int y = 0; y < 200; y++) {
			String a = getDocument("misc");
			for (int x = 0; x < y; x++) {
				String c = getDocument("i" + x);
			}
			String b = getDocument("misc");
			assert (a.equals(b));
		}
	}

}
